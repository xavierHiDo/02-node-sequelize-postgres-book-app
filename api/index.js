import config from 'dotenv';
import express from 'express';
import bookRoutes from './server/routes/BookRoutes';

config.config();

const app = express();

app.use(express.json()); //Body reading and parsing
const port = process.env.PORT || 8000;

app.use('/api/v1/books', bookRoutes);
// when a random route is inputed
app.get('*', (req, res) => res.status(200).send({
   message: 'Welcome to this API.'
}));

app.listen(port, () => {
   console.log(`Server is running on PORT ${port}`);
});

export default app;